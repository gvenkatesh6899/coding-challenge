package com.hcl.pojo;

import java.util.Scanner;

public class EmployeeManagementApp {

	public static void main(String[] args) {
		
		Employee emps[]=new Employee[3]; // allocating memory to an array
		Scanner sc=new Scanner(System.in);
		for(int i=0;i<3;i++)
		{
			System.out.println("Enter Employee's Id");
			int id=sc.nextInt();
			System.out.println("Enter Employee's Name");
			String name=sc.next();
			Employee e=new Employee(id, name); // initialize every employee object before adding them into array
			emps[i]=e;
		}
		
		for(int i=0;i<3;i++)
		{
			System.out.println("Id = "+emps[i].getId());
			System.out.println("Name = "+emps[i].getName());
		}
		
		
		
		
	}

}
